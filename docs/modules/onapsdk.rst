onapsdk package
===============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   onapsdk.aai
   onapsdk.cds
   onapsdk.configuration
   onapsdk.sdnc
   onapsdk.so
   onapsdk.utils

Submodules
----------

onapsdk.constants module
------------------------

.. automodule:: onapsdk.constants
   :members:
   :undoc-members:
   :show-inheritance:

onapsdk.esr module
------------------

.. automodule:: onapsdk.esr
   :members:
   :undoc-members:
   :show-inheritance:

onapsdk.msb module
------------------

.. automodule:: onapsdk.msb
   :members:
   :undoc-members:
   :show-inheritance:

onapsdk.multicloud module
-------------------------

.. automodule:: onapsdk.multicloud
   :members:
   :undoc-members:
   :show-inheritance:

onapsdk.nbi module
------------------

.. automodule:: onapsdk.nbi
   :members:
   :undoc-members:
   :show-inheritance:

onapsdk.onap\_service module
----------------------------

.. automodule:: onapsdk.onap_service
   :members:
   :undoc-members:
   :show-inheritance:

onapsdk.sdc module
------------------

.. automodule:: onapsdk.sdc
   :members:
   :undoc-members:
   :show-inheritance:

onapsdk.sdc\_element module
---------------------------

.. automodule:: onapsdk.sdc_element
   :members:
   :undoc-members:
   :show-inheritance:

onapsdk.sdc\_resource module
----------------------------

.. automodule:: onapsdk.sdc_resource
   :members:
   :undoc-members:
   :show-inheritance:

onapsdk.service module
----------------------

.. automodule:: onapsdk.service
   :members:
   :undoc-members:
   :show-inheritance:

onapsdk.vendor module
---------------------

.. automodule:: onapsdk.vendor
   :members:
   :undoc-members:
   :show-inheritance:

onapsdk.version module
----------------------

.. automodule:: onapsdk.version
   :members:
   :undoc-members:
   :show-inheritance:

onapsdk.vf module
-----------------

.. automodule:: onapsdk.vf
   :members:
   :undoc-members:
   :show-inheritance:

onapsdk.vid module
------------------

.. automodule:: onapsdk.vid
   :members:
   :undoc-members:
   :show-inheritance:

onapsdk.vsp module
------------------

.. automodule:: onapsdk.vsp
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: onapsdk
   :members:
   :undoc-members:
   :show-inheritance:
